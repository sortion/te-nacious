#!/bin/awk -f 

# List all chromosomes from a fasta file 
# Usage: ./list_chromosomes.awk <sequences.fasta>

BEGIN {
	FS = ";"
}

/^>/ {
	location = $2
	split(location, location_array, ":")
	chromosome = location_array[1]
	gsub("loc=", "", chromosome)
	gsub(" ", "", chromosome)
	if (length(chromosome) <= 2) {
		chromosome = substr(chromosome, 1, 1)
		if (chromosome in chromosomes) {
			chromosomes[chromosome]++ # may be used to count genes per chromosome
		} else {
			chromosomes[chromosome] = 1
		}
	}
}

END {
	for (chromosome in chromosomes) {
		print chromosome
	}
}