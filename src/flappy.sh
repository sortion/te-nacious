#!/usr/bin/env bash
: '
Generate FLAP plot with automatic sequence length retrieval from fasta sequence
Uses already generated blast format 6 output.

Usage: ./flappy.sh <family_name> <copy_id> <output_file>
Example: ./src/flappy.sh "HMS-Beagle" "FBti0019130" "HMS-Beagle_FBti0019130_FLAP.png"
'
FAMILY_NAME="$1"
COPY_ID="$2"
OUTPUT="$3"
BLAST_DIR="./data/out/align"
REFERENCE_FILE="${BLAST_DIR}/${FAMILY_NAME}.fasta"
COPIES_FILE="${BLAST_DIR}/${FAMILY_NAME}-all.fasta"
BLAST_FILE="${BLAST_DIR}/${FAMILY_NAME}.blast"
COPY_LENGTH="$(./src/fasta_sequence_length.awk -v target=${COPY_ID} ${COPIES_FILE})"
REFERENCE_LENGTH="$(./src/fasta_sequence_length.awk -v idx=1 ${REFERENCE_FILE})"

PLOT_TITLE="${FAMILY_NAME} transposon family reference alignment against ${COPY_ID} copy"

python3 ./tools/flap/flap.py --blast ${BLAST_FILE} --title "${PLOT_TITLE}" --query-name "ref" --subject-name "${COPY_ID}" --query-length "${REFERENCE_LENGTH}" --subject-length "${COPY_LENGTH}" --center --color-map rainbow --output "${OUTPUT}" --min-identity 70