#!/bin/awk -f 

# List all chromosomes from a list of genes with location 
# Usage: ./list_chromosomes.awk <genes.txt>

BEGIN {
	FS = " "
	# Set of chromosomes	
}

{
	chromosome = $2
	gsub("loc=", "", chromosome)
	chromosome = substr(chromosome, 1, 2)
	if (chromosome in chromosomes) {
		chromosomes[chromosome]++ # may be used to count genes per chromosome
	} else {
		chromosomes[chromosome] = 1
	}
}

END {
	for (chromosome in chromosomes) {
		print chromosome
	}
}