#!/usr/bin/env bash
: ' Merge qmd notebooks, from date written in header'

list_qmds() {
    local input_folder
    input_folder="$1"
    find $input_folder -name '*.qmd'
}

read_date() {
    local qmd
    qmd=$1
    grep -m 1 -E '^date:' $qmd | sed -E 's/^date: (.*)$/\1/'
}

sorted_by_date() {
    local qmds
    qmds=$@
    for qmd in $qmds; do
        echo "$(read_date $qmd) $qmd"
    done | sort -b -t- -k1n -k2n -k3n -r | cut -d ' ' -f 2-
}

today() {
    date +%Y-%m-%d
}

merge_notebooks() {
    local files
    files=$@
    echo "Notebooks - Bioinfo Project - Samuel Ortion"
    echo "merged on $(today)"
    for file in $files; do 
        echo "== $file =="
        cat $file
        echo
    done
}

usage() {
    echo "Usage: $0 <input_folder> <output_file>"
}

input_folder="$1"
output_file="$2"

if [[ -z "$input_folder" ]]; then
    usage
    exit 1
fi
if [[ -d "$input_folder" ]]; then
    echo "Input folder: $input_folder"
else
    echo "Input folder does not exist: $input_folder"
    exit 1
fi
if [[ -z "$output_file" ]]; then
    usage
    exit 1
fi
if [[ -f "$output_file" ]]; then
    echo "Output file exists: $output_file"
    exit 1
fi

qmds=$(list_qmds "$input_folder")
sorted_qmds=$(sorted_by_date "$qmds")
merge_notebooks "$sorted_qmds" > "$output_file"