#!/bin/awk -f
# Extract transposon sequences from a FASTA file containing multiple transposon sequences
# Usage: ./extract_family_sequences.awk -v target="gypsy12" input.fasta > output.fasta
# where target is the family name to extract
# Detect the beginning of a new sequence of the target family
/^>/ {
    name = $4
	gsub("name=", "", name)
	split(name, name_array, "{}")
	gsub(" ", "", name_array[1])
	family = name_array[1]
	if (family ~ target) {
        print $0
        flag = 1
    } else {
        flag = 0
    }
}

# Print nucleotide sequence only if it is a part of a hit of the target family
/^[^>]/ && flag == 1 {
    print $0
}