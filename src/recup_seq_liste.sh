#! /bin/bash

# auteur: Carène Rizzon
# avril 2023
# script pour sélectionner dans un fichier de séquences au format fasta une liste spécifique de séquences
# qui correspond aux séquences d'une même famille ET
# utilisation: ./recup_seq_liste_projet2023.sh dmel-all-transposon-r6.38.fasta
# changer le nom d'intéret dans le code

SEQ=$1    # fasta file 

awk  '

{
if($1~">")
	{
	copier=0;
	
	# récupération du nom de la famille ET dans la variable nom
	tmp=substr($4,6,length($4)-6);
	split(tmp,aa,"\{") ; 
	nom=aa[1];
	
	if (nom=="copia") # changer copia si  vous analysez une autre famille
		{
		print $0;
		copier=1
		}
	};
if($1!~">")
	{
	if(copier==1)
		print $0
	}
}

' $SEQ
