#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create a TSV table with data from BLAST HSPs,
with following columns:
- `numero_accession_te1` 
- `numero_accession_te2` 
- `pident` (best hit)
- `length` (best hit)
- `coverage` (with all hits)
- `evalue` (best hit)
- `number_of_hits`
"""

import pandas as pd
import os
from Bio import SeqIO

def family_names(reference_file):
    try:
        family_names_df = pd.read_csv(reference_file, sep=' ', header=0, comment="#")
        family_names = family_names_df['Nom_ET'].tolist()
    except:
        raise Exception(f"ERROR: error while reading {reference_file}")
    return family_names

def load_blast_hsps(blast_hsps_file):
    """
    Load BLAST HSPs from a TSV file.
    :param blast_hsps_file: path to the TSV file
    :return: pd.DataFrame with BLAST HSPs
    """
    blast_hsps = pd.read_csv(blast_hsps_file, sep='\t', comment="#")
    blast_hsps.columns = ["queryid", "subjectid", "pident", "length", "mismatch", "gapopen", "qstart", "qend", "sstart", "send", "evalue", "bitscore"]
    return blast_hsps

def coverage(hsp_coordinates, reference_length):
    """
    Compute alignment coverage for a given set of HSP coordinates.
    We assume that the coverage cannot be greater than 100% (that is 1.0).
    :param hsp_coordinates: list of tuples (start, end)
    :param reference_length: length of the reference sequence
    """
    def merge_overlaps(coordinates, start_index=0):
        """
        Merge overlapping coordinates.
   
        :param coordinates: list of tuples (start, end)
        :return list of tuples (start, end)

        ref: https://stackoverflow.com/a/49072633
        """ 
        for i in range(start_index, len(coordinates) - 1):
            if coordinates[i][1] > coordinates[i+1][0]:
                new_start = coordinates[i][0]
                new_end = max(coordinates[i][1], coordinates[i+1][1])
                coordinates[i] = (new_start, new_end)
                del coordinates[i+1]
                return merge_overlaps(coordinates, i)
        return coordinates
    
    # Merge overlapping coordinates
    merged_coordinates = merge_overlaps(hsp_coordinates)
    # Compute coverage
    coverage = sum([end - start for start, end in merged_coordinates]) / reference_length
    coverage = min(coverage, 1.0)
    return coverage

def blast_stats(blast_hsps, query_id, subject_id, reference_length):
    """
    Compute BLAST statistics for a given query and subject.
    :param blast_hsps: pd.DataFrame with BLAST HSPs
    :return: tuple (pident, length, coverage, evalue, number_of_hits)
    """
    coordinates = []
    best_length = 0
    hits_count = 0
    for i, hsp in blast_hsps.iterrows():
        if hsp["queryid"] == query_id and hsp["subjectid"] == subject_id:
            coordinates.append((hsp["qstart"], hsp["qend"]))
            if hsp["length"] > best_length:
                best_length = hsp["length"]
                best_pident = hsp["pident"]
                best_evalue = hsp["evalue"]
            hits_count += 1
    cov = coverage(coordinates, reference_length)
    return best_pident, best_length, cov, best_evalue, hits_count

def reference_id_and_length(reference_file):
    bio_sequence = SeqIO.read(reference_file, "fasta")
    id = bio_sequence.id
    return id, len(bio_sequence)

def family_reference_lengths(family_names):
    reference_lengths = {}
    for family_name in family_names:
        reference_file = f"./data/out/align/{family_name}.fasta"
        id, length = reference_id_and_length(reference_file)
        reference_lengths[id] = length
    return reference_lengths

def main():
    REFERENCE_FILE = "./data/references/liste_familles_ET_fichierref.txt"
    df_collected = pd.DataFrame(columns=["numero_accession_te1", "numero_accession_te2", "pident", "length", "coverage", "evalue", "number_of_hits"])
    for family_name in family_names(REFERENCE_FILE):
        reference_fasta = f"./data/out/align/{family_name}.fasta"
        if not os.path.exists(reference_fasta):
            print(f"WARNING: {reference_fasta} does not exist")
            continue
        id, length = reference_id_and_length(reference_fasta)
        blast_hsps_file = f"./data/out/align/{family_name}.blast"
        if not os.path.exists(blast_hsps_file):
            continue
        try:
            blast_hsps = load_blast_hsps(blast_hsps_file)
        except:
            print(f"WARNING: error while reading {blast_hsps_file}")
            continue
        for subject_id in blast_hsps["subjectid"].unique():
            pident, length, coverage, evalue, number_of_hits = blast_stats(blast_hsps, id, subject_id, length)
            if number_of_hits > 0:
                df_collected.loc[len(df_collected)] = [id, subject_id, pident, length, coverage, evalue, number_of_hits]
    # Map a very small evalue to 0.0 (to avoid issues with pgsql real precision)
    df_collected["evalue"] = df_collected["evalue"].apply(lambda x: 0.0 if x < 1e307 else x) 
    # Save results
    df_collected.to_csv("./data/out/db/blast_hsps.tsv", sep='\t', index=False, float_format="%1.2e")

if __name__ == "__main__":
    main()