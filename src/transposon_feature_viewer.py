#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Draw a transposon feature diagram, from GenBank file
"""

from dna_features_viewer import BiopythonTranslator
import matplotlib.pyplot as plt
import click

class MyCustomTranslator(BiopythonTranslator):
    """Custom translator implementing the following theme:

    - CDS features are blue
    - terminator features are green
    - other features are gold
    """

    def compute_feature_color(self, feature):
        if feature.type == "CDS":
            return "blue"
        elif feature.type == "terminator":
            return "green"
        else:
            return "gold"

    # def compute_feature_label(self, feature):
    #     return BiopythonTranslator.compute_feature_label(self, feature)

    def compute_filtered_features(self, features):
        """Do not display the source feature."""
        return [
            feature for feature in features
            if feature.type != "source"
        ]

@click.command()
@click.version_option()
@click.option("-i", "--genbank", 'genbank_file', type=click.Path(exists=True), help="GenBank file containing features (LTR for instance)")
@click.option("-o", "--output", 'output_file', type=click.Path(), help="Output figure filename", default = None)
@click.option("-t", "--title", 'title', type=str, help="Figure title", default = None)
def cli(genbank_file, output_file, title):
    graphic_record = MyCustomTranslator().translate_record(genbank_file)
    ax, _ = graphic_record.plot(figure_width=10, strand_in_label_threshold=7)
    # ax.figure.tight_layout()
    if title is not None:
        ax.set_title(title)
    if output_file is not None:
        ax.figure.savefig(output_file, dpi=300)
    else:
        plt.show()

def main():
    cli()

if __name__ == '__main__':
    main()