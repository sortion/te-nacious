#!/usr/bin/env bash
: '
Create a TSV table with data from BLAST HSPs,
with following columns:
- `numero_accession_te1` 
- `numero_accession_te2` 
- `pident` (best hit)
- `length` (best hit)
- `coverage` (with all hits)
- `evalue` (best hit)
- `number_of_hits`
'

# Retrieve family names
family_names() {
    awk '
    NR > 1 {
        print $1
    }
    ' ./data/references/liste_familles_ET_fichierref.txt
}

# Retrieve reference sequence length
sequence_length() {
    local file_name
    file_name="$1"
    ./src/fasta_sequence_length.awk -v idx=1 $file_name
}

# Retrieve reference sequence id
sequence_id() {
    local fasta
    fasta="$1"
    awk '
    NR == 1 && /^>/ {
        sequence_id=$1
        gsub(/^>/, "", sequence_id)
        print sequence_id
    }' $fasta
}

# Retrieve best HSP from BLAST output
# And compute coverage
blast_stats() {
    local blast
    blast="$1"
    local query_id
    query_id="$2"
    local subject_id
    subject_id="$3"
    local reference_length
    reference_length="$4"
    awk -v query_id=$query_id -v subject_id=$subject_id -v reference_length=$reference_length '

    BEGIN {
        best_length = 0
        number_of_hits = 0
        OFS = "\t"
    }
    # Avoid BLAST comments and run only on query_id and subject_id related lines
    /^[^#]/ && $1 == query_id && $2 == subject_id {
        hsp_length = $4
        if (hsp_length > best_length) {
            best_length = hsp_length
            best_evalue = $11
            best_pident = $3
        }
        number_of_hits++
    }
    END {
        coverage = 0
        if (number_of_hits > 0) {
            print query_id, subject_id, best_pident, best_length, coverage, best_evalue, number_of_hits
        }
    }
    ' $blast
}

for family_name in $(family_names); do
    if [[ ! -e "./data/out/align/$family_name.fasta" ]]; then
        echo "No reference sequence for $family_name" > /dev/stderr
        continue
    elif [[ ! -e "./data/out/align/$family_name.blast" ]]; then
        echo "No BLAST output for $family_name" > /dev/stderr
        continue
    fi
    # Retrieve reference sequence length
    reference_length=$(sequence_length "./data/references/$family_name.fasta")
    # Retrieve reference sequence id
    reference_id=$(sequence_id "./data/references/$family_name.fasta")
    # Retrieve best HSP from BLAST output
    # And compute coverage
    blast_stats "./data/out/blast/$family_name.blast" $reference_id $reference_id $reference_length
done