---
title: Starting blocks analysis
---

## Data Summary

### File list

in `./data` directory:	


| File name | Description |
|-----------|-------------|
|dmel-all-translation-r6.38.fasta |  proteome |
|dmel-all-transposon-r6.38.fasta | transposon sequences |
|Dmelanogaster_6_38_positions_genes.list |  gene positions |

All three files data concern the *Drosophila melanogaster* species, version r6.38.

### Quick statistics

1. Number of chromosomes:

* Using the genes positions list:
```bash
awk -F ' ' '{ print $2 }' data/Dmelanogaster_6_38_positions_genes.list \
	| sort | uniq | wc -l
```

⚠️  This command takes into account all different first word in `loc=` field including for instance:
- `2L`, `2R`
- rDNA
- mitochondrion_genome
- and even "211000022279188", whose significance is unknown.

```bash
./src/list_chromosomes.awk ./data/Dmelanogaster_6_38_positions_genes.list
```


* Using the proteome file:
```bash
./src/list_chromosomes_fasta.awk \
	./data/dmel-all-translation-r6.38.fasta \
	| sort -n
```

```text
X
Y
2
3
4
```

In *D. melanogaster*, there are 3 autosomes pairs (2, 3, 4), and a pair of sexual chromosomes (X and Y).

2. Total number of transposable elements (TEs) identified in `dmel-all-transposon-r6.38.fasta` file:

```bash
awk '
/^>/ {
	name = $1
	gsub(">", "", name)
	split(name, name_array, " ")
	id = name_array[1]
	print id
}' ./data/dmel-all-transposon-r6.38.fasta | sort | uniq | wc -l
```

```text
5392
```

3. Number of TEs per chromosome:

```bash
awk -F  ';' '
/^>/ {
	# Extract chromosome ID
	location = $2
	gsub("loc=", "", location)
	split(location, location_array, ":")
	chromosome = location_array[1]
	counter[chromosome]++
}
END {
	for (chromosome in counter) {
		print chromosome, counter[chromosome]
	}
}
' ./data/dmel-all-transposon-r6.38.fasta | sort -d -k 1
```

| Chromosome (arm) | Number of TEs |
|------------------|---------------|
| 2L | 919 |
| 2R | 1358 |
| 3L | 1056 |
| 3R | 631 |
| 4 | 541 |
| X | 887 |

Interestingly enough, there is no TE reported on the Y chromosome.

4. Number of TEs families:

Let us first extract all TE IDs from the `dmel-all-transposon-r6.38.fasta` file:

```bash
egrep "^>" ./data/dmel-all-transposon-r6.38.fasta > ./data/out/dmel-all-transposon-r6.38.list
```

Then, we can extract the TE family ID from the TE ID:

```bash
awk -F ';' '
{
	name = $3
	gsub("name=", "", name)
	split(name, name_array, "{}")
	gsub(" ", "", name_array[1])
	family = name_array[1]
	print family
} ' ./data/out/dmel-all-transposon-r6.38.list \
	| sort -d | uniq > ./data/out/dmel-all-transposon-r6.38.families.list
```


Number of TE families (raw):

```bash
wc -l ./data/out/dmel-all-transposon-r6.38.families.list
```

```text
126
```

Number of copies of each TE family:

```bash
awk '
BEGIN{
	FS = ";"
	OFS = "\t"
}
{
	name = $3
	gsub("name=", "", name)
	split(name, name_array, "{}")
	gsub(" ", "", name_array[1])
	family = name_array[1]
	counter[family]++
}
END {
	for (family in counter) {
		print family, counter[family]
	}
}
 ' ./data/out/dmel-all-transposon-r6.38.list > ./data/out/dmel-all-transposon-r6.38.families.count
```

Let us plot this data:

```{r, echo = FALSE, message = FALSE}
#| label: fig:transposon_families_count
#| fig-cap: "Number of copies of each TE family. Families with more than 50 copies are shown. Some sub families (e.g of Gypsy, Invader, etc.) are grouped together. "

library(ggplot2)
library(dplyr)
transposon_families_count_df <- read.table("../../data/out/dmel-all-transposon-r6.38.families.count", header = FALSE)
colnames(transposon_families_count_df) <- c("TE Family", "Copies")
# head(transposon_families_count_df)

group_families <- function(families, family, df) {
	# Get all families from family names vector 
	# and group their counts in a single family
	df <- df %>%
		mutate(`TE Family` = ifelse(`TE Family` %in% families, family, `TE Family`))
	df <- df %>%
		group_by(`TE Family`) %>%
		summarise(Copies = sum(Copies))
	return(df)
}

# Group families
df <- transposon_families_count_df

gypsies <- sapply(1:12, function(x) paste0("gypsy", x))
df <- group_families(c(gypsies, "gypsy"), "gypsy", df)

invaders <- sapply(1:5, function(x) paste0("invader", x))
df <- group_families(c(invaders, "invader"), "invader", df)

df <- group_families(c("jockey", "jockey2"), "jockey", df)

stalker <- sapply(1:4, function(x) paste0("Stalker", x))
df <- group_families(c(stalker, "Stalker"), "Stalker", df)

# Filter only TE families with more than 10 copies
df <- df %>%
	filter(Copies > 50)

ggplot(data = df, aes(x = `TE Family`, y = Copies)) +
	geom_bar(stat = "identity") +
	ggtitle("Number of copies of each TE family") +
	theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1))
```

## Transposable Elements Classification

- DNA 
- SINE
- LINE
- LTR

```{r, echo = FALSE, message = FALSE}
#| label: fig:class-families-count
#| fig-cap: "Number of copies of commulated TE families per Superfamilies"
families2superfamilies <- read.table('../../data/family_to_superfamily.tab', sep = '\t', header = FALSE)
colnames(families2superfamilies) <- c('TE Family', 'TE Superfamily')
df <- transposon_families_count_df %>%
	left_join(families2superfamilies, by = 'TE Family') %>%
	group_by(`TE Superfamily`) %>%
	summarise(Copies = sum(Copies))

transposon_superfam_count_df <- df

# head(df)

ggplot(data = df, aes(x = `TE Superfamily`, y = Copies)) +
	geom_bar(stat = "identity") +
	ggtitle("Number of copies of commulated TE families per class") +
	theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1))
```

```{r, echo = FALSE, message = FALSE}
#| label: fig:te-superfamilies-class
#| fig-cap: "Number of copies per superfamiles, per class. Class of interest are indicated in color."

INTEREST_FAMILIES <- c("DNA", "LTR", "non-LTR")
superfamilies2class <- list(
	DNA = c("DNA", "Helitron", "Transib", "piggy", "Mariner"),
	LTR = c("Copia", "Gypsy", "BEL", "Circe"),
	nonLTR = c("Jockey", "LINE", "LOA") # LOA -> baggins
)

match_class <- function(superfamily) {
	for (class in names(superfamilies2class)) {
		# regex
		for (classSuperFamily in superfamilies2class[[class]]) {
			if (grepl(classSuperFamily, superfamily) | grepl(toupper(classSuperFamily), toupper(superfamily))) {
				return(class)
			}
		}
	}
	return (NA)
}

match_class("Gypsy") # check if it works

df <- transposon_superfam_count_df %>%
	mutate(`TE Class` = sapply(`TE Superfamily`, match_class))

ggplot(df, aes(x = `TE Superfamily`, y = Copies, fill = `TE Class`)) +
	geom_bar(stat = "identity") +
	ggtitle("Number of copies per superfamiles, class hightlighted") +
	theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1))
# ggsave("../media/figures/TE-superfamilies-class-count.png", width = 10, height = 5)
```

### Family choice reloaded

#### Source file description

|File|Description|
|---|---|
| `dmel-all-transposon-r6.38.fasta` | Transposon sequences from Drosophila melanogaster |
| `data/references/*` | Transposon families reference sequences either in EMBL or FASTA format |
| `data/references/liste_familles_ET_fichierref.txt` | Liste des familles et des fichiers de référence


```bash
head data/references/liste_familles_ET_fichierref.txt
```

```text
Nom_ET Classe Type_principal Nom_fichier_ref
1360 classII transposon 1360.txt
1731 classI LTR 1731.txt
17.6 classI LTR 17.6.txt
297 classI LTR 297.txt
3S18 classI LTR 3S18.txt
412 classI LTR 412.txt
BS classI no-LTR BS.txt
BS3 classI no-LTR BS3_Repbase.txt
BS4 classI no-LTR BS4_Repbase.txt
```

### A more precise analyzis of class distribution

```{r, message = FALSE}
#| fig-label: fig:transposon-class-distribution-ref
#| fig-caption: Distribution of transposon hits number by type, with the overall type stated in color.

library(ggplot2)
library(dplyr)

# Get hits count for each families
family_count_df <- read.table("../../data/out/dmel-all-transposon-r6.38.families.count", sep = "\t", header = FALSE)
colnames(family_count_df) <- c("Nom_ET", "count")


# Get class for each families
family_class_df <- read.table("../../data/references/liste_familles_ET_fichierref.txt", sep = " ", header = TRUE)

head(family_class_df)

family_df <- family_class_df %>% 
  left_join(family_count_df, by = "Nom_ET") %>% 
  mutate(count = ifelse(is.na(count), 0, count))

# Filter for above 20 hist
family_df <- family_df %>% 
  filter(count > 20) %>% filter(!is.na(Type_principal))

head(family_df)

ggplot(data = family_df, aes(x = Nom_ET, y = count, fill = Type_principal)) + 
  geom_bar(stat = "identity") + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5))
```

### Choice of families

```{r, message = FALSE}
#| fig-label: fig:transposon-class-distribution-dna
#| fig-caption: Distribution of transposon of each class (DNA, LTR, non-LTR) hits number by family.

library(tidyverse)

family_count_df <- read.table("../../data/out/dmel-all-transposon-r6.38.families.count", sep = "\t", header = FALSE)
colnames(family_count_df) <- c("Nom_ET", "count")

family_class_df <- read.table("../../data/references/liste_familles_ET_fichierref.txt", sep = " ", header = TRUE)

family_df <- family_class_df %>% 
  left_join(family_count_df, by = "Nom_ET") %>% 
  mutate(count = ifelse(is.na(count), 0, count))
  

family_DNA_df <- family_df %>% 
  filter(Type_principal == "transposon") %>% 
  arrange(desc(count))

family_LTR_df <- family_df %>%
  filter(Type_principal == "LTR") %>% 
  arrange(desc(count))

family_nonLTR_df <- family_df %>%
  filter(Type_principal == "no-LTR") %>% 
  arrange(desc(count))

# Plot DNA transposon with bar ordered by desc counts using reorder
ggplot(data = family_DNA_df, aes(x = reorder(Nom_ET, -count), y = count)) + 
  geom_bar(stat = "identity") + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5)) +
  ggtitle("DNA transposon") +
  xlab("Family") +
  ylab("Copy count")

# Plot LTR transposon with bar ordered by desc counts using reorder
ggplot(data = family_LTR_df, aes(x = reorder(Nom_ET, -count), y = count)) + 
  geom_bar(stat = "identity") + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5)) +
  ggtitle("LTR transposon families distribution") +
  xlab("Family") +
  ylab("Copy count")

# Plot non-LTR transposon with bar ordered by desc counts using reorder
ggplot(data = family_nonLTR_df, aes(x = reorder(Nom_ET, -count), y = count)) + 
  geom_bar(stat = "identity") + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5)) +
  ggtitle("non-LTR transposon families distribution") +
  xlab("Family") +
  ylab("Copy count")
```


| Type | Family |
|------|--------|
| DNA | hopper |
| LTR retrotransposon | HMS-Beagle |
| no-LTR retrotransposon | BS | 
: Choice of transposon families for the further detailed analysis

