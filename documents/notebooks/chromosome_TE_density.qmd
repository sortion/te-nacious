---
title: |
    Transposable Element Chromosome Density
author:
    - Samuel Ortion
format: pdf
engine: knitr
pdf-engine: lualatex
---

## Chromosome Plot using GenomeTools

1. Download the GFF3 file from FlyBase

2. Install [GenomeTools](http://genometools.org)

3. Extract a subset of the GFF3 file

```{bash, eval=FALSE}
head - ./data/dmel-all-filtered-r6.38.gff > ./data/dmel-all-filtered-r6.38_subset.gff
```

4. Draw the genes

```{bash, eval=FALSE}
gt sketch output.png ./data/dmel-all-filtered-r6.38_subset.gff
```
⚠️ Unfortunatlly, this method returns an empy figure.

## Chromosome Plot using ChromPlot

```{r}
library(chromPlot)
```

(See Appendix A for ChromPlot installation instructions)


## References

[@GtGFFplot] Dessiner vos gènes à partir d'un GFF3 <https://bioinfo-fr.net/dessiner-vos-genes-a-partir-dun-gff3?hilite=gff>
[@ChromPlot] ChromPlot - BioConductor <https://bioconductor.org/packages/devel/bioc/vignettes/chromPlot/inst/doc/chromPlot.pdf>

## Appendix A: ChromPlot Installation

```{r, eval=FALSE}
# First install BiocManager (Bioconductor) if not already installed
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
# Then install chromPlot
BiocManager::install("chromPlot")
```