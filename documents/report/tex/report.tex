\documentclass[
    onside,
    12pt
]{chameleon/sty/bioinfo-report}

\input{preamble.tex}
\input{definitions.tex}
\addbibresource{references.bib} %

\title{TE-nacious, Une analyse des éléments transposables chez \textit{Drosophila melanogaster}}
\author{Curie Jordane Kakpovi \thanks{Licence Génomique Biologie Informatique} \and Samuel Ortion \thanks{Licence Double Sciences de la vie - Informatique}}
\date{2023}

% Set PDF Metadata
\hypersetup{
    pdftitle={TE-nacious, Une analyse des éléments transposables chez \textit{D. melanogaster}},
    pdfauthor={Curie Jordane Kakpovi, Samuel Ortion},
    pdfsubject={Rapport Projet Bioinformatique},
    pdfkeywords={bioinformatique, unix, sql, transposable elements, fruitfly},
    pdflang={french}
}

\begin{document}
\input{layout/titlepage.tex}

{
\hypersetup{hidelinks}
\tableofcontents
}

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTRODUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

Les éléments transposables ont été découverts pour la première fois chez le maïs par Barbara McClintock dans les années 40\cite{mcclintock_origin_1950,mcclintock_mutable_1947}. Depuis lors, ils ont été identifiés dans une grande variété d'organismes, allant des bactéries aux plantes et animaux. Les éléments transposables sont des séquences d'ADN qui peuvent se déplacer et se dupliquer dans les génomes, ce qui les rend très mobiles. En fait, ils peuvent représenter une part importante du génome, allant jusqu'à 90\% notamment chez certaines plantes.

Bien que les éléments transposables ne soient pas indispensables à la survie, ils peuvent avoir un impact significatif sur l'évolution des génomes. Par exemple, ils peuvent affecter l'expression de gènes voisins, provoquer des mutations et des réarrangements chromosomiques, et former des pseudogènes dans le génome hôte. Cependant, dans certains cas, les éléments transposables peuvent également être bénéfiques pour l'organisme hôte.

Il existe deux grandes classes d'éléments transposables : les transposons à ADN et les rétrotransposons. Les transposons à ADN se déplacent directement dans le génome, tandis que les rétrotransposons se copient d'abord en ARN, qui est ensuite rétrotranscrit en ADN avant d'être insérés dans le génome. Ces mécanismes de transposition peuvent avoir des conséquences différentes pour le génome hôte. Par exemple, les transposons à ADN peuvent provoquer des cassures de l'ADN lors de leur insertion, ce qui peut conduire à des mutations et des réarrangements chromosomiques. Les rétrotransposons, en revanche, peuvent créer de nouvelles copies de gènes dans le génome, ce qui peut contribuer à la diversité génétique.

\paragraph{Transposon à ADN} Par un mécanisme \textquote{cut-and-paste} (couper-coller), un
élément présent à un emplacement donné du génome est excisé et inséré par transfert
de brin à un autre.Par la suite sont réparés, par l'hôte, les sites excisés au cours de la
transposition. Les séquences se terminent par des ITR (\textit{Inverted Terminal Repeats}).

\paragraph{Rétrotransposons à ARN} Par un mécanisme réplicatif \textquote{copy-and-paste}
(copier - coller), un élément présent à un emplacement donné du génome est copié et
collé à un autre emplacement. Cet élément est transcrit en ARN qui sera rétrotranscrit en ADN par une transcriptase inverse. l’ ADN sont ensuite réinséré dans le génome. On distingue deux types de rétrotransposons à ARN:
\begin{itemize}
    \item Les rétrotransposons avec LTR qui ont des séquences se terminant par des LTR (Long Terminal Repeats);
    \item Les rétrotransposons sans LTR qui sont des séquences courtes sans LTR commençant par un promoteur interne et se terminant par une queue poly-A.
\end{itemize}

Provenant les uns des autres par transpositions les transposons de structures similaires,
avec le même mécanisme de transposition sont regroupés par familles.

Dans ce projet nous nous focaliserons sur le génome de la mouche du vinaigre (\textit{Drosophila
melanogaster}), organisme modèle.

Existerait-il, chez \textit{Drosophila melanogaster}, des copies d’éléments transposables qui, à proximité d’un gène, influencent l’expression de ce gène ?
La distribution des familles d'éléments transposables est-elle aléatoire, le long des chromosome ? 
Les éléments transposables sont ils présent aux endroits pauvres en gènes ?

Le but de cette étude est d'utiliser des outils de bio-informatique pour caractériser les copies de plusieurs familles d'éléments transposables chez la drosophile. Il s'agit de déterminer si ces copies sont complètes ou incomplètes par rapport à la référence, si elles sont dégradées ou non, et de conclure sur leur éventuelle fonctionnalité. 

Nous construisons une base de données relationnelle d'éléments transposables pour que les biologistes intéressés par ces questions puissent y trouver quelques éléments de réponses.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATERIALS AND METHOD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Matériel et méthode}

\subsection{Sources des données}

Les données de séquences proviennent de la révision \texttt{r6.36} du génome de la drosophile \textit{Drosophila melanogaster}, de la banque de données FlyBase\footnote{Lien FTP pour les données aux format FASTA: \url{https://ftp.flybase.net/releases/FB2021_01/dmel_r6.38/fasta/}}.

Les tailles des chromosomes ont été récupérées sur le site du NCBI\footnote{Publication 6 du génome de la drosophile, annotations, NCBI - Genomes: \url{https://www.ncbi.nlm.nih.gov/data-hub/genome/GCF_000001215.4/}}.

Les fichiers de séquences des copies de références proviennent de diverses sources, telles que les banques de données RebBase, Dfam et du NCBI. 

Nous choisissons de travailler plus spécifiquement sur trois familles d'éléments transposables présents chez \textit{Drosophila melanogaster}, pour chacuns des trois types principaux:
\begin{itemize}
    \item Transposon à ADN: \textit{hopper};
    \item Rétrotransposon à LTR: \textit{HMS-Beagle};
    \item Rétrotransposon sans LTR: \textit{BS}.
\end{itemize}

La \cref{fig:dna-features-scheme} représente sur un schéma réalisé avec DNA-Features-Viewer\footnote{DNA-Features-Viewer: \url{https://edinburgh-genome-foundry.github.io/DnaFeaturesViewer/}}, les annotations de séquences des références des trois familles choisies, à partir de leur fiche GenBank.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.9\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/images/scheme/features_hopper.pdf}
        \caption{\textbf{\textit{hopper}} (Transposon)}
    \end{subfigure}
    \begin{subfigure}{0.9\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/images/scheme/features_HMS-Beagle.pdf}
        \caption{\textbf{\textit{HMS-Beagle}} (Rétrotransposon à LTR)}
    \end{subfigure}
    \begin{subfigure}{0.9\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/images/scheme/features_BS.pdf}
        \caption{\textbf{\textit{BS}} (Rétrotransposon sans LTR)}
    \end{subfigure}
    \caption{\textbf{Schémas des régions annotées de la séquence de référence des trois familles d'éléments transposables choisies.} Réalisés avec DNA-Feature-Viewer\cite{noauthor_dna_features_viewer_nodate}.}
    \label{fig:dna-features-scheme}
\end{figure}

\subsection{Outils}

\paragraph{Seqret} Cet utilitaire du package EMBOSS permet la lecture et la conversion de séquences biologiques. 

\paragraph{BLAST+}
\subparagraph{blastn} Cet outil utilise une matrice de similarité pour calculer des scores d'alignement et retourner un alignement local de deux séquences d'acides nucléiques, en se basant sur une heuristique. Il fournit un score pour chaque alignement trouvé et utilise ce score pour donner une évaluation statistique de la pertinence de cet alignement (probabilité qu'il soit dû au hasard)\cite{altschul_basic_1990}.

\subparagraph{makeblastdb} L'outil \texttt{makeblastdb} produit des bases de données BLAST à partir de fichiers FASTA. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BLAST ANALYZIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Analyses blast et annotations des copies d'éléments transposables}

Un alignement BLAST des copies de références de nos familles d'éléments transposables d'intérêts contre les copies de ces familles présentes dans le génome de la drosophile a été réalisé.
À partir de ces alignements au format tabulé, un script Python nous a permis de dessiner des schémas des régions conservées entre copie et référence \footnote{FLAP: \url{https://framagit.org/BioloGeeks/bioinfo/flap}}.

\paragraph{\textit{hopper}} La copie \texttt{FBti0020139} de la famille \textit{hopper} présente une insertion par rapport à la copie de référence, dans la région répétée. Deux portions de séquences sont cependant plutôt conservés (approximativement 85\% d'identité à la référence) (\cref{fig:flap-hopper-FBti0020139}). 

Il semble difficile d'affirmer si cette copie est suffisamment conservée pour être capable de se transposer.
À l'inverse, la copie \texttt{FBti0019555} ne présente pas de grandes insertions ou délétions (\cref{fig:flap-hopper-FBti0019555}; la différence de taille s'expliquant par les gaps présents dans l'alignement local recouvrant l'essentiel de la séquence.
La copie est identique à environ 90\% à la référence de référence. Il est donc probable que cette copie soit en capacité de se transposer.

\paragraph{\textit{HMS-Beagle}} La copie \texttt{FBti0019143} correspond à la référence. Les régions LTR matchent, formant la croix rouge du schéma. Cette copie est sans doute fonctionnelle.

La copie \texttt{FBti0063232} quant à elle est moins conservée. Certaines régions codantes sont altérées. Il semble que la région LTR en aval soit elle aussi dégradée. Il est possible que cette copie ne soit pas capable de se transposer.


\paragraph{\textit{BS}} La copie \texttt{FBti0020125} présente deux régions fortements similaires avec la copie de référence. Une particularité intéressante de cette copie est que la séquence correspondant probablement à l'ORF inconnu et à la reverse transcriptase (cf. \cref{fig:dna-features-scheme}(c)) a changé de brin d'ADN: l'alignement est renversé.
Le pourcentage d'identité élevée pour les deux régions HSPs semble indique que cette copie est bien conservée. Elle est probablement viable.

% Hopper

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{media/images/figures/flap/hopper_ref_vs_FBti0020139_copy.pdf}
    \caption{\textbf{Schéma de l'alignement local de la référence du transposon à ADN \textit{hopper} contre la copie \texttt{FBti0020139}}}
    \label{fig:flap-hopper-FBti0020139}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{media/images/figures/flap/hopper_ref_vs_FBti0019555_copy.pdf}
    \caption{\textbf{Schéma de l'alignement local de la référence du transposon à ADN \textit{hopper} contre la copie \texttt{FBti0019555}}}
    \label{fig:flap-hopper-FBti0019555}
\end{figure}

% \begin{figure}[H]
%     \centering
%     \includegraphics[width=\textwidth]{media/images/figures/flap/hopper_ref_vs_FBti0019800_copy.pdf}
%     \caption{\textbf{Schéma de l'alignement local de la référence du transposon à ADN \textit{hopper} contre la copie \texttt{FBti0019800}}}
%     \label{fig:flap-hopper-FBti0019800}
% \end{figure}

% \begin{figure}[H]
%     \centering
%     \includegraphics[width=\textwidth]{media/images/figures/flap/hopper_ref_vs_FBti0020381_copy.pdf}
%     \caption{\textbf{Schéma de l'alignement local de la référence du transposon à ADN \textit{hopper} contre la copie \texttt{FBti0020381}}}
%     \label{fig:flap-hopper-FBti0020381}
% \end{figure}

% HMS-Beagle

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{media/images/figures/flap/HMS-Beagle_ref_vs_FBti0019143_copy.pdf}
    \caption{\textbf{Schéma de l'alignement local de la référence du transposon LTR \textit{HMS-Beagle} contre la copie \texttt{FBti0019143}}}
    \label{fig:flap-HMS-Beagle-FBti0019143}
\end{figure}

% \begin{figure}[H]
%     \centering
%     \includegraphics[width=\textwidth]{media/images/figures/flap/HMS-Beagle_ref_vs_FBti0062666_copy.pdf}
%     \caption{\textbf{Schéma de l'alignement local de la référence du transposon LTR \textit{HMS-Beagle} contre la copie \texttt{FBti0062666}}}
%     \label{fig:flap-HMS-Beagle-FBti0062666}
% \end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{media/images/figures/flap/HMS-Beagle_ref_vs_FBti0063232_copy.pdf}
    \caption{\textbf{Schéma de l'alignement local de la référence du transposon LTR \textit{HMS-Beagle} contre la copie \texttt{FBti0063232}}}
    \label{fig:flap-HMS-Beagle-FBti0063232}
\end{figure}

% BS

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{media/images/figures/flap/BS_ref_vs_FBti0020125_copy.pdf}
    \caption{\textbf{Schéma de l'alignement local de la référence du transposon no-LTR \textit{BS} contre la copie \texttt{FBti0020125}}}
    \label{fig:flap-BS-FBti0020125}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATABASE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Base de données relationnelle}

\subsection{Objectifs et spécifications}

En bio-informatique, les bases de données ont un rôle crucial car elles permettent de stocker, d'organiser et de gérer les données . Voici quelques objectifs spécifiques des bases de données en bio-informatique:
Uniformisation de la saisie et standardisation des traitements (ex: tous les résultats de consultation sous forme de listes et de tableaux), contrôle immédiat de la validité des données, partage de données entre plusieurs traitements ce qui limite la redondance des données.
En ce qui concerne notre projet on souhaite stocker des informations sur les copies d'éléments transposables de \textit{Drosophila melanogaster}. Ces informations incluent localisation des gènes et éléments transposables sur lechromosome, la taille, la famille d'appartenance et leur orientation. De plus, les copies de référence pour les familles d'éléments transposables seront également stockées, avec leur numéro d'accession du gène et leur fonction du gène. Les résultats des analyses Blast entre les copies et les références seront également enregistrée, en prenant en compte le score, le pourcentage d'identité, la taille du meilleur alignement local, la couverture d'alignement pour la copie et le nombre d'alignements locaux (copie/référence). En outre, la classification des éléments transposables sera prise en compte, en particulier pour les trois groupes de familles choisis pour le projet. 

\subsection{Modèle conceptuel - Schéma entité-association}
 
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{./media/images/scheme/Correction_EA.drawio.png}
    \caption{Correction du Schéma Entité - Association}
    \label{fig:EA}
\end{figure}

Le schéma conceptuel permet la description des données d’un domaine du monde réel en termes de types d’objets et de liens logiques indépendants de toute représentation en machine, correspondant à une vue canonique globale de la portion d’entreprise modélisée.

Entité: Une entité est un objet, une chose concrète ou abstraite qui
peut être reconnue distinctement et qui est caractérisée par son unicité.

Type-entité: un type-entité désigne un ensemble d’entités qui
possèdent une sémantique et des propriétés communes.

Association: Une association (ou relation) est un lien entre plusieurs
entités.

Type-association: Un type-association (ou type-relation) désigne un
ensemble d’associations qui possèdent les mêmes caractéristiques.
Le type-association décrit un lien entre plusieurs type-entités. Les
associations de ce type-association lient des entités de ces types
entités.

Dans le cas de notre projet nous avons eu quatre type-entités Chromosome, Copie, Famille ET, Gène ayant respectivement pour clés primaires Numéro, Numéroaccession, Nom et Numéro accession. Ils sont  reliés entre eux par des types associations. Le type association \textquote{one-to-many}  entre les type-entités  Copie et Famille ET. Les cardinalités de l'associaton 0,1 signifie une Copie appartient à 0 ou au plus 1 Famille ET,la cardinalité 0,n du coté de famille ET signifie qu'une famille peut appartenir à une ou plusieurs copie.

Au niveau du type association many-to-one entre les type-entités Chromosome et gène les cardinalités 0,n du coté du Chromosome notifie qu'un chromosome peut être localiser au niveau de 0 à n Gène et les cardinalités 0,1 au niveau du gène nous permets de dire qu'un gène peut etre localiser au niveau de 0 à au plus 1 Chromosome. 
Nous avons également un type association many-to-many réflexif au niveau du type entité Copie. Ce type association permet de vérifier le nombre de pattes du type association elle permet de créer une relation spécifique du même nom dans le modèle logique relationnel. Il faudra créer autant de de clés étrangères que de pattes du type-association, les clés étrangers doivent avoir des noms différents car tous les attributs d'une relation doivent avoir des noms différents.

\subsection{Modèle logique relationnel}
\begin{math}\\
CHROMOSOME(\underline{NumeroChromosome}, TailleChromosome) \\
GENE(\underline{NumeroAccessionGene}, TailleGene, FonctionGene) \\
LOCALISATION\_GENE(\underline{\#NumeroAccessionGene, \#NumeroChromosome}, Debut, Fin, Orientation) \\
COPIE(\underline{NumeroAccessionCopie}, FamilleCopie, TailleCopie) \\
LOCALISATION\_COPIE(\underline{\#NumeroAccessionCopie, \#NumeroChromosome}, Debut, Fin, Orientation) \\
FAMILLE(\underline{NomFamille}, ClasseFamille) \\
BLAST\_HIT(\underline{\#NumeroAccessionQuery, \#NumeroAccessionSubject},  Pident, Length, Coverage, Number) \\[1em]
\Dom(GENE(NumeroAccessionGene)) \supseteq \Dom(LOCALISATION\_GENE(NumeroAccessionGene)) \\
\Dom(CHROMOSOME(NumeroChromosome)) \supseteq \Dom(LOCALISATION\_GENE(NumeroChromosome)) \\
\Dom(COPIE(NumeroAccessionCopie)) \supseteq \Dom(LOCALISATION\_COPIE(NumeroAccessionCopie)) \\
\Dom(CHROMOSOME(NumeroChromosome)) \supseteq \Dom(LOCALISATION\_COPIE(NumeroChromosome)) \\
\Dom(FAMILLE(NomFamille)) \supseteq \Dom(COPIE(CopieFamille)) \\
\Dom(COPIE(NumeroAccessionCopie)) \supseteq \Dom(BLAST\_HIT(NumeroAccessionQuery)) \\
\Dom(COPIE(NumeroAccessionCopie)) \supseteq \Dom(BLAST\_HIT(NumeroAccessionSubject)) \\
\end{math}

\subsection{Implémentation SQL du schéma}

Nous utilisons le système de gestion de base de données relationnelle PostgreSQL.

Voir l'\cref{apdx:database-schema}, pour le code utilisé pour l'implémentation du schéma de la base.

\subsection{Bilan des données inclues}

109 des 125 familles d'éléments transposables répertoriés dans le jeu de données ont été inclues dans la base de données.

Le tableau \cref{tab:table-sizes} résume les tailles des différentes tables de notre base de données.

Il y a autant d'entrées pour les gènes que pour leur localisation. De façon similaire, il y a autant d'entrées dans \texttt{transposable\_elements} que dans \texttt{te\_location}, plus les 109 copies de références, pour lesquelles la localisation peut manquer, et qui sont donc absentes de \texttt{te\_location}.

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
         \toprule
         \textbf{Table} & \textbf{Taille} \\
         \midrule
         chromosomes & 8  \\
         families & 125 \\
         genes & 17797 \\
         transposable\_elements & 5501 \\
         blast\_hits & 2246 \\
         \bottomrule
    \end{tabular}
    \caption{\textbf{Nombre d'entrées dans la base de données, pour chacune des tables implémentées}.}
    \label{tab:table-sizes}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{media/images/figures/plots/te_distribution_chromosome_genes.png}
    \vspace{-2cm}
    \caption{\textbf{Positions des éléments transposables et des gènes le long des chromosomes de \textit{Drosophila melanogaster}.} Les points rouges indiquent la position des centromères des chromosomes R/L. Les points ont été agités (\textit{jitter}) sur l'axe des ordonnées pour mieux observer la répartition. Figure inspirée de \cite{kaminker_transposable_2002} (fig. 2).}
    \label{fig:te-genes-positions}
\end{figure}

\subsection{Requêtes de la base de données}

\paragraph{Conservation des copies d'ET et proximité avec un gène}

\begin{minted}{sql}
SELECT te.numero_accession_te AS te_id, 
    te.family_name AS family_name, 
    family.family_class, 
    MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position)) AS distance, 
    pident, 
    coverage
FROM transposable_elements te, 
    te_location te_loc, 
    gene_location gene_loc, 
    blast_hits blast, 
    families family
WHERE te.numero_accession_te = te_loc.numero_accession_te
AND family.family_name = te.family_name
AND te_loc.numero_chromosome = gene_loc.numero_chromosome
AND te.numero_accession_te = blast.numero_accession_te2
GROUP BY te.numero_accession_te, te.family_name, family.family_class, pident, coverage;
\end{minted} 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{media/images/figures/plots/te_gene_distance_blast_coverage.pdf}
    \caption{\textbf{Couverture des alignements copie ET versus référence en fonction de la distance de l'ET au gène le plus proche}}
    \label{fig:distance-gene-vs-coverage}
\end{figure}

La distribution des couvertures des alignements BLAST des éléments transposables copies contre référence est représentés \cref{fig:blast-coverage-hist}. De même pour la distance au gène le plus proche (\cref{fig:te-gene-distance-hist}).

\subparagraph{Influence de l'orientation} Il pourrait y avoir une influence de la position sur le brin sens / antisens de l'élément transposable par rapport au gène le plus proche sur la conservation de l'élément transposable. Pour tester cette hypothèse, une requête filtrant les distances gène - TE sur le même brin d'ADN peut être réalisée:
\begin{minted}{sql}
SELECT te.numero_accession_te AS te_id, 
    te.family_name AS family_name, 
    family.family_class,
    MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position,           gene_loc.end_position)) AS distance, 
    pident, 
    coverage
FROM transposable_elements te,
        te_location te_loc,
        gene_location gene_loc,
        blast_hits blast,
        families family
    WHERE te.numero_accession_te = te_loc.numero_accession_te
    AND family.family_name = te.family_name
    AND te_loc.numero_chromosome = gene_loc.numero_chromosome
    AND te.numero_accession_te = blast.numero_accession_te2
    AND te_loc.orientation = gene_loc.orientation
    GROUP BY te.numero_accession_te, te.family_name, family.family_class, pident, coverage;
\end{minted}

La structure du nuage de points des couvertures en fonction des distances minimales sur le même brin d'ADN ne semble pas fondamentalement différente, de celle avec les deux brins confondus (cf. \cref{distance-gene-vs-coverage}).

\paragraph{Distribution des tailles d'éléments transposables d'une même famille}

\begin{minted}{sql}
SELECT copy.numero_accession_te as te_id, 
    family.family_name, 
    family.family_class, 
    copy.te_length as copy_length, 
    ref.te_length as ref_length
FROM transposable_elements copy,
    transposable_elements ref,
    families family
WHERE copy.family_name = family.family_name
  AND copy.family_name = ref.family_name
  AND ref.reference;
\end{minted}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{media/images/figures/plots/te_lengths_ratio.pdf}
    \caption{\textbf{Distribution des ratios des longueur des copies des éléments transposables par rapport à la copie de référence.} Les copies aux longueurs supérieure à 2 fois la longueur de la référence ne sont pas représentées.}
    \label{fig:te-lengths-ratios}
\end{figure}

Un graphique inspiré de Kaminker \textit{et al.} (2002)  représente la position des éléments transposables le long des  chromosomes (\cref{fig:te-genes-positions}) \cite{kaminker_transposable_2002}.

La densité des éléments transposable et des gènes a été calculés sur fenêtre glissante le long des chromosomes (\cref{fig:te-density}).

\section{Conclusion et Discussion}

\paragraph{Résultats}

Une base de données sur les éléments transposables a été mise en place. Les résultats d'analyses blast pour les copies les plus conservées d'une centaine de familles d'ET ont été incorporés dans cette base. La base de données a permis le requêtage SQL des données générées.

L'estimation de l'état des éléments transposables constitue un défi important. La seule lecture des statistiques de l'alignement local permet de se faire une idée sur la conservation des copies, mais l'établissement d'un critère seuil entre copie transposable ou non constitue un élément non définitivement résolu.

Une répartition non homogène des éléments transposables le long des chromosomes a pu être illustrée (\cref{fig:te-density} et \cref{fig:te-genes-positions}), les zones pauvres en gènes l'étant aussi en éléments transposables, à large échelle; bien que la densité en éléments transposables mesurée par fenêtre glissante et celle des gènes ne soient pas significativement corrélées (corrélation de Pearson).

Il n'a pas pu être montré de relation linéaire forte entre conservation des éléments transposables (assimilée à la couverture ou au pourcentage d'identité du meilleur HSP de l'alignement BLAST) et proximité avec un gène (\cref{fig:distance-gene-vs-coverage}).

\paragraph{Difficultés et Stratégies}

Le parti a été pris d'utiliser une base de données postgres sur un de nos ordinateurs portables tournant sous une Fedora (GNU/Linux). La copie de la base de données via un dump n'a pas pu être réalisée directement, à cause de légers problèmes de compatibilité entre les versions et les configurations de postgres sous les deux environnements testés. Parmis ceux-ci figure par exemple l'impossibilité d'utiliser l'instruction \mintinline{sql}{IF NOT EXISTS} SQL sur la version de postgres installée sur le serveur Debian mis à disposition des étudiants. Nous avons pu contourner ces problèmes en transmettant les données au format tabulé, au lieu du \textit{dump} sql et en adaptant un peu le code sql du schéma.

Certaines difficultés ont été rencontrées lors de l'utilisation du sous-système Linux de Windows (WSL), ainsi que CygWin. Ces difficultés ont compliqué la progression du projet sur un de nos ordinateurs portables équipé de Windows, en dehors des sessions à l'université.

L'utilisation de Git et GitLab a permis le partage et la synchronisation du code utilisé lors de ce projet, même si la collaboration par ce biais aurait pu être davantage développée\footnote{Dépôt TE-nacious sur FramaGit: \url{https://framagit.org/UncleSamulus/te-nacious/}}.

La progression du projet a été suivie par l'intermédiaire de notebooks \href{https:/quarto.org/}{Quarto}, rédigés au fur et à mesure et compilés régulièrement pour mise en ligne sur le GitLab Pages du projet\footnote{GitLab Pages hébergeant les notebooks Quarto compilés: \url{https://unclesamulus.frama.io/te-nacious/}}.

L'essentiel des tâches répétitives réalisées en ligne de commande ont été scriptées que ce soit en bash, python ou awk. Cela nous a permis d'inclure presque la totalité des données présentes dans le jeu initial sans trop de difficultés.

\section{Perspectives} 

\paragraph{Compléter la base de données} Il pourrait être intéressant d'inclure les annotations fonctionnelles des gènes au sein de la base de données. Une analyse de la relation entre famille d'éléments transposables proches et fonction des gènes, pourrait dès lors être envisagée.
L'ajout des données de séquences au format FASTA pourrait être mise en place. Cela pourrait se faire, par exemple via l'ajout d'un champ \mintinline{sql}{BLOB} (\textit{Binary Large Object})\footnote{Un test d'insertion de la séquence de référence de la famille 1360 dans une table dédiée a été réalisé avec succès.}.
Il pourrait être intéressant de manipuler le paramètre \texttt{--max\_hsps} de \texttt{blastn}, pour tenter de récupérer davantage de données sur les copies plus dégradées. En effet, dans l'état actuel des choses, seule une petite fraction des copies disponibles dans le jeu de données ont droit à une annotation dans la table \mintinline{sql}{blast_hits}.
Les quelques familles absentes dans notre base de données mériteraient que l'on s'y intéresse un peu plus, et que l'on identifie les fichiers manquant pour compléter le jeu de données.
Une extension de la base de données pourrait être étudiée en vu de l'inclusion de données concernant d'autres génomes.

\paragraph{Une interface} Une interface plus conviviale au requêtage de la base de données et une mise à disposition des chercheurs pourrait être une évolution intéressante. 

\paragraph{Automatisation} Une automatisation plus poussée des processus d'annotation, d'insertion dans la base de données, etc. permettrait d'envisager de répéter ce processus plus aisément à chaque nouvelle révision du génome de la drosophile.
Pour ce faire, une utilisation d'un gestionnaire de workflow, de type Nextflow ou Snakemake serait bienvenue, pour rendre reproductible ce type de procédés.

\paragraph{Idées d'analyses} 

\subparagraph{Structure 3D des chromosomes} De nos jours, des techniques permettent d'étudier la structure 3D des chromosomes, par exemple par la méthode 3C / Hi-C. Il pourrait être intéressant d'associer l'étude des éléments transposables avec celle de la conformation 3D du génome. Il a par exemple été montré que les éléments transposables  pouvaient jouer un rôle important dans la conformation 3D des chromosomes durant l'embryogenèse chez la souris \cite{kruse_transposable_2019}, peut être qu'une étude similaire pourrait être envisagée chez la drosophile.

\subparagraph{Recombinaison de l'ADN} Une comparaison de la répartition des éléments transposables avec les régions où le taux de recombinaison est élevé pourrait être une utilisation intéressante de notre base de données \cite{rizzon_recombination_2002}.

\subparagraph{Phylogénie des copies} Une étude du lignage des copies d'une même famille d'éléments transposables pourrait être réalisée. Des phylogénie entre les familles, ou entre plusieurs espèces basées sur les copies de références sont déjà présentées dans la littérature \cite{clark_phylogenetic_1997}.

\newpage
\nocite{*}
{
\raggedright
\printbibliography
}

\pagebreak

\begin{appendix}
\part*{Annexes}

\section{Implémentation SQL du schéma de la base de données}
\label{apdx:database-schema}

\inputminted{sql}{./media/code/schema.sql}

\section{Implémentation de la fonction \mintinline{sql}{distance_to_gene}}

\begin{minted}{sql}
CREATE FUNCTION distance_to_gene(te_start INTEGER, te_end INTEGER, gene_start INTEGER, gene_end INTEGER) RETURNS INTEGER AS $$
    DECLARE
        distance INTEGER;
    BEGIN
        IF te_start > gene_end THEN
            distance := te_start - gene_end;
        ELSIF te_end < gene_start THEN
            distance := gene_start - te_end;
        ELSE
            distance := 0;
        END IF;
        RETURN distance;
    END;
$$ LANGUAGE plpgsql;
\end{minted}


\section{Figures supplémentaires}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.9\textwidth}
        \centering
        \caption{Transposons ADN}
        \includegraphics[width=\textwidth]{media/images/figures/plots/TE-DNA-families-count.pdf}
    \end{subfigure}
    \begin{subfigure}{0.9\textwidth}
        \centering
        \caption{Rétrotransposons à LTR}
        \includegraphics[width=\textwidth]{media/images/figures/plots/TE-LTR-families-count.pdf}
    \end{subfigure}
    \begin{subfigure}{0.9\textwidth}
        \centering
        \caption{Rétrotransposon sans LTR}
        \includegraphics[width=\textwidth]{media/images/figures/plots/TE-nonLTR-families-count.pdf}
    \end{subfigure}
    \caption{\textbf{Répartitions du nombre de copies pour les trois types d'éléments transposables étudiés, dans le génome de la drosophile.}}
    \label{fig:count-te-copies-per-types}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{media/images/figures/plots/blast-coverage-hist.pdf}
    \caption{\textbf{Distribution des couvertures des alignements copie d'élément transposable \textit{versus} référence}}
    \label{fig:blast-coverage-hist}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{media/images/figures/plots/blast-distance-hist.pdf}
    \caption{\textbf{Distribution des distances minimales élément transposable - gène}}
    \label{fig:te-gene-distance-hist}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.95\textwidth]{media/images/figures/plots/te_density_chromosome_3R.png}
    \caption{\textbf{Nombre d'éléments transposables sur fenêtres glissantes, chromosome 3R de \textit{D. melanogaster}}}
    \label{fig:te-density}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{media/images/figures/plots/te_gene_distance_blast_coverage_same_orientation.pdf}
    \caption{\textbf{Couverture de l'alignement d'un élément transposable avec la copie de référence de sa famille en fonction de la distance de l'élément transposable avec le gène le plus proche, sur le même brin d'ADN.}}
    \label{fig:distance-gene-vs-coverage-same}
\end{figure}

\end{appendix}

\end{document}
