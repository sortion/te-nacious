# Éléments transposables

Ressemblent à des gènes. Contiennent tout ce qu'il faut pour synthétiser des protéines.

L'annotation des éléments transposables est importante dans l'annotation des génomes.

## Découverte

Chez le maïs, années 1950, Barbara Mc Clintock, éléments sautant, responsable de la couleur des grains de maïs.

Des mutations apparaissent et induisent un changement de couleur des grains, des éléments génétiques "sautent" dans le génome.

Les pétunias, et autres plantes présentent le même type d'éléments génétiques.

Chez *D. melanogaster*, les chromosomes des glandes salivaires ont des chromosomes polytenes, les chromosomes sont collés.

Quand le chromosome n'était pas séquencé, on pouvait quand 

Les chromosomes polytene étalés, les transposons marqués par radioactivité, permettent de détecter les localisation des transposons sur le chromosome.

## Répétitions dans le génome

- Minisatellites / Microsatellites: forme des répétitions dans le génome $\rightarrow$ Très répétés;

- Gènes $\rightarrow$ peu répétés;

- Éléments transposable $\rightarrow$ moyennement répétés (assez peu chez la drosophile, représente 90% du génome chez certaines plantes)

## Mécanismes de transposition des éléments transposables

### Couper-coller: Transposition via ADN

Une insertion dans le chromosome, l'élément transposable code pour plusieurs protéines:

1. capable de couper l'élément transposable;

2. capable d'intégrer l'élément transposable ailleur.

L'élément a juste bougé.

#### Réplication de l'ADN

Pendant la réplication de l'ADN, si l'élément tranposable bouge au bon moment, deux copies peuvent rester dans le génome.

### Copier-coller: Transposition via ARN

1. Copier: Transcription de l'élément transposable;

2. Transcriptase inverse transforme l'ARNm en ADNc;

3. Les intégrases intègrent l'ADNc dans le génome.

### Intérêt de l'étude des éléments transposables

Lors de l'insertion d'un élément transposable il peut advenir:

1. Destruction d'un CDS (si le transposon s'insère dans le CDS, la descendance peut ne pas être viable);

2. Insertion dans un intron (le transposon peut être dans un gène) (on peut l'observer dans la descendance);

### Structure des éléments transposables

- Promoteur;

- De toutes les tailles (de 200 bp à 10000 bp)

#### Transposons:

*Transpositions via ADN*

(80% du génome humain en terme de longueur d'ADN)

- ITR *inverted terminal repeats* (reverse complémentaires de part et d'autres du transposon) (contient le promoteur)

- transposase (ORF "entre un Stop et un Stop")

### Rétrotransposons

*Transpositions par ARN*

transposons autonomes.

aussi appelés chez l'home des *LINE*

- LTR *Long Terminal Repeats* (de part et d'autres, dans le même sens) (contient le promoteur)

- *gag* code pour la capside;

- pog *RT*

- env

: Structure d'un rétrovirus (de type HIV)
$\rightarrow$ Tous les génomes eucaryotes contiennent des rétrovirus endogènes.

### Rétrotransposons sans LTR

*Transpositions par ARN*

Aussi appelés chez l'home *SINE* (beaucoup impliqués dans les cancer)

transposons non autonomes.

- Pas de LTR

- Promoteur interne

- RT

- Queue poly(A)

*frameshift*



### Éléments transposables intégrés dans d'autres éléments transposables



## Événements dû aux éléments transposables

Parfois le gène est transcrit via le signal (promoteur) du transposons qui l'intègre, ce qui peut résulter en une augmentation de l'expression du gène.

Il a été montré que les transposables pouvaient avoir un rôle dans certains cancer, en augmentant le niveau d'expression d'un oncogène.

Certains gènes sont "recrutés" par l'hôte: par exemple la télomérase de la drosophile provient d'un élément transposable.

Hypothèse le plus probable: L'insuline 2 de la souris a été produite via une transcriptase inverse issue d'un transposon.

Certains gènes sont ainsi dupliqués par transcription inverse.

Dans  la plupart du temps, cependant de tels événement produisent des gènes inactifs (pseudogènes) et disparaissent au fur et à mesure de l'évolution.

Réarrangement chromosomique dû à une recombinaison ectopique entre deux éléments transposables.

### Variation dans la structure des éléments transposables:

Très diverse, et parfois compliquées.

#### Classes:

- Classe II: transposition à ADN;

- Classe I: transposition à ARN;

Une même famille provient d'un même élément transposable, et diverge au fur et à mesure que les mutations s'accumulent, et à mesure des événements de transpositions.

Deux transposons d'une même famille peuvent interagir.

Les familles diffèrent en :

- La distribution suivant les familles;

- Le nombre de copie;

- Les mécanismes de transposition.

*Question biologique* Est-ce que la distribution des familles chez la drosophile est aléatoire, le long du chromosome ?

- Les ET sont-ils présent où il n'y a pas de gène (chromatine condensée, etc.) ?

- Est-ce que les ET sont inversement distribués par rapport au gènes ?

### Évolutions des éléments transposables

### Questions sur les éléments transposables

- Comment les éléments envahissent-ils les génomes et les populations ?
  
  - *junk DNA*;
  
  - ADN égoïste;
  
  - "parasite";
  
  - ? 
  
  - Recrutement: point de vue plus nuancé

- Apporte de l'adaptabilité, variabilité génétique;

- Quelle est l'origine des éléments transposables ?

- Quelles sont les interactions entre élément transposable et gènes de l'hôte ?

- Quels liens évolutifs existent-ils entre des éléments transposables ?

Les éléments transposables produisent des petits ARN régulateurs. Les ET auraient alors un rôle important dans la régulation des gènes.

On peut faire des phylogénies sur les protéines communes des éléments transposables (par exemple la *reverse transcriptase*).

- Quelles sont les caractéristiques des éléments transposables aux voisinages des gènes qui influencent leur expression ?
  
  - Promoteur en amont du gène;
  
  - Conservation, copies non dégradées à proximité de gènes.

### Pression de sélection

On peut montrer qu'il existe un lien entre la recombinaison et de la pression de sélection.

Types de pression de sélection:

- Pression négative sur les transposons qui s'intègrent aux CDS des gènes;